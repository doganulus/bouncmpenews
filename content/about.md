+++
title = "Bogazici University Computer Engineering"
date = "2014-04-09"
+++

Bogazici University, founded in 1863, is the top ranking public university in Turkey according to the central university exam results. The Computer Engineering Department (BU-CmpE) ranks as the top CmpE department in Turkey according to the Higher Education Council's number of publications per faculty list, with its 21 full-time faculty members, 9 research labs, more than 250 graduate students and 650 undergraduate students. The department is internationally recognized for its education and research activities (QS World University Ranking for BU & QS University Rankings: EECA 2016 in comparison). The Computer Engineering Program is accredited by ABET.  

The mission statement of Bogazici University School of Engineering is as follows:

> The mission of the Bogazici University School of Engineering is to educate individuals for careers of leadership and innovation in industry, government and educational institutions; to expand the engineering knowledge base through original research; to contribute through professional services towards more prosperous and sustainable society.

In light of the above mission statement, Department of Computer Engineering has established the following mission for itself:

Department of Computer Engineering Mission Statement:

> The mission of the Computer Engineering program is to graduate economically, socially and ethically conscious engineers that are equipped with the necessary analytical, mathematical, decision-making, computing, communication, teamwork and leadership skills that they can use to creatively design, implement, manage, maintain and improve a wide spectrum of computer hardware, software and information systems in the modern society.

Additionally, the program educational objectives of our department has been established as follows:

Department of Computer Engineering graduates will

* Pursue the practice of computer engineering in various industrial sectors not only at the national but also at the international level.
* Pursue advanced studies and engage in research activities in world-class institutions.
* Pursue a life-long career of personal and professional growth in the rapidly changing computing world with ethical consciousness and global awareness.
* Be able to assume innovator, executive, leadership, and entrepreneurial roles in their careers.

List of Computer Engineering Student Outcomes:
1. An ability to identify, formulate, and solve complex engineering problems by applying principles of engineering, science, and mathematics.
2. An ability to apply engineering design to produce solutions that meet specified needs with consideration of public health, safety, and welfare, as well as global, cultural, social, environmental, and economic factors.
1. An ability to communicate effectively with a range of audiences.
1. An ability to recognize ethical and professional responsibilities in engineering situations and make informed judgments, which must consider the impact of engineering solutions in global, economic, environmental, and societal contexts.
1. An ability to function effectively on a team whose members together provide leadership, create a collaborative and inclusive environment, establish goals, plan tasks, and meet objectives.
1. An ability to develop and conduct appropriate experimentation, analyze and interpret data, and use engineering judgment to draw conclusions.
1. An ability to acquire and apply new knowledge as needed, using appropriate learning strategies.
1. Knowledge of probability and statistics, including applications appropriate to computer engineering.
1. Knowledge of mathematics through differential and integral calculus and basic sciences.
1. Knowledge of engineering topics including computing science necessary to analyze and design complex electrical and electronic devices, software, and systems containing hardware and software components.
1. Knowledge of discrete mathematics.

## History