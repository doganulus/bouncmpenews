+++
title = "CmpE students ranked first in Inzva programming contest"
description = ""
tags = [
"go",
"golang",
"templates",
"themes",
"development",
]
date = "2014-04-02"
categories = [
"Development",
"golang",
]
image = "artist.jpg"
+++
Boğaziçi University Computer Engineering students Ekrem Bal, Murat Ekici, and Yasin Kaya, were ranked first in Online programming contest by Inzva.